#include "Parser.h"
#define ls 1
#define setenv 2
#define printenv 3
#define exit 4
#define RD_CMD 0
#define RD_ARG 1

Parser::Parser(string cmdStr){
    status = 0;
    isFirstCmd = true;   
    sstr = stringstream(cmdStr);
}

queue<Task> Parser::parse(){
    queue<char*> command;
    string buf;
    while(sstr >> buf){
        while(buf.back()<=32){
            buf.pop_back();
        }
        if(buf[0] == '|' || buf[0] == '!'){
            Task task(command);
            if(buf[0] == '!')
                task.toErr = true;
            
            if(buf[1] > '0' && buf[1] < '9' && buf[2]!='+'){
                int pipeNum = atoi(str2char(buf)+1);
                task.pipeCnt = pipeNum;
            }else if(buf[1] > '0' && buf[1] < '9' && buf[2] =='+'){
                int sum = 0;
                char *num = new char[256];
                int cnt = 0;
                for(int i=0; i<buf.length(); i++){
                    if(buf[i] > '0' && buf[i] < '9'){
                        num[cnt++] = buf[i];
                    } else {
                        num[cnt] = '\0';
                        sum += atoi(num);
                        num = new char[256];
                        cnt = 0;
                    }
                }
                num[cnt] = '\0';
                sum += atoi(num);
                printf("sum: %d\n", sum);
                task.pipeCnt = sum;
            } else {
                task.pipeCnt = 1;
            }

            commands.push(task);
            command = queue<char*>();
        } else if(buf[0] == '>'){
            Task task(command);
            string filetmp;
            sstr >> filetmp;    
            task.toFile = true;
            task.filename = str2char(filetmp);
            printf("filename: %s\n", task.filename);
            commands.push(task);
            command = queue<char*>();
        } else {
            command.push(str2char(buf));
        }
    }   
    if(!command.empty()){
        Task task(command);
        task.pipeCnt = 0;
        commands.push(task);
        task.toPrint = true;
    }

    return commands;
}

char* Parser::str2char(string str){
    char* s = new char[256];
    for(int i=0; i< str.length(); i++){
        s[i] = str[i];
    }
    s[str.length()] = '\0';
    return s;
}

