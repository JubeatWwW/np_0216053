#include "TaskManager.h"

#define PIPE_IN 0
#define PIPE_OUT 1

TaskManager::TaskManager(){
    head = 0;
    for(int i = 0; i< 1021; i++){
        tasks[i] = queue<Task>();
    }
}

void TaskManager::pushTask(queue<Task> input, int newsocketfd){
    while(!input.empty()){
        Task currTask = input.front();
        input.pop();
        printf("head: %d\n", head);
        printf("task queue: %d\n\n", tasks[head].size());
        int ret = currTask.execute(tasks[head], newsocketfd); 
        if(ret == 1){
            if(currTask.pipeCnt > 0){
                int pushNum = (head + currTask.pipeCnt )% 1020;
                tasks[pushNum].push(currTask);
                printf("push task to: %d\n\n", head+currTask.pipeCnt);
            } else{
                char *line = new char[256];
                memset(line, 0, sizeof(line));
                close(currTask.pipeOut[PIPE_OUT]);
                int len = 0;
                if(currTask.toFile){
                    fstream file;
                    file.open(currTask.filename, ios::out | ios::trunc);
                    
                    while((len = read(currTask.pipeOut[PIPE_IN], line, 255)) > 0){
                        //printf("\ntest:\n\n%s", line);
                        file.write(line, len);
                        memset(line, 0, sizeof(line));
                    }
                    file.close();
                } else {
                    while((len = read(currTask.pipeOut[PIPE_IN], line, 255)) > 0){
                        //printf("\ntest:\n\n%s", line);
                        write(newsocketfd, line, len);
                        memset(line, 0, sizeof(line));
                    }
                }
                printf("\nno delay\n");
            }
        } else {
            if(ret == -1){
                close(newsocketfd);

            }
        }

        head++;
        if(head > 1019){
            head = 0;
        }
    }
}
