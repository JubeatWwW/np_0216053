#include "Task.h"
#define PIPE_IN 0
#define PIPE_OUT 1

Task::Task(queue<char*> cmd){
    command = cmd;
    pipeCnt = 0;
    toPrint = false;
    toFile = false;
    toErr = false;
}

int Task::execute(queue<Task> pipeInput, int newsockfd){
    int pid;
    chdir("/u/cs/102/0216053/ras");
    vector<char*> params;
    char* cmd = command.front();
    //printf("cmd: %s\n", cmd);


    while(!command.empty()){
        params.push_back(command.front());
        //printf("params: %s\n", command.front());
        command.pop();
    }
    
    if(strcmp(cmd, "setenv") == 0){
        //printf("%s\n%s\n", params[1], params[2]);
        setenv(params[1], params[2], 1);
        //printf("setenv\n");
        return 0;
    } else if(strcmp(cmd, "printenv") == 0){
        //printf("getenv: %s\n", params[1]);
        char *getvar = getenv(params[1]);
        char *prefix = strcat(params[1], "=");
        getvar = strcat(prefix, getvar);
        write(newsockfd, strcat(getvar, "\n"), strlen(getvar)+1);
        //printf("printenv\n");
        return 0;
    } else if(strcmp(cmd, "exit") == 0){
        printf("exit\n");
        return -1;
    }

    if(command.empty())
        params.push_back((char*)0);
    
    if(pipe(pipeOut) < 0){
        perror("pipeOut: pipeerr: ");
    }
    
    if(!pipeInput.empty()){
        Task task = pipeInput.front();
        pipeInput.pop();
        while(!pipeInput.empty()){
            Task tmp = pipeInput.front();
            pipeInput.pop();
            close(tmp.pipeOut[PIPE_OUT]);
            char *line = new char[1001];
            int tmpLen = 0;
            while((tmpLen = read(tmp.pipeOut[PIPE_IN], line, 1000)) > 0){
                write(task.pipeOut[PIPE_OUT], line, tmpLen);
            } 
            close(tmp.pipeOut[PIPE_IN]);
        }
        
        if (dup2(task.pipeOut[PIPE_IN], STDIN_FILENO) < 0) {
            perror("In Task: redirecting stdin");
            close(task.pipeOut[PIPE_IN]);
        }
        close(task.pipeOut[PIPE_IN]);
        close(task.pipeOut[PIPE_OUT]);
    }
    printf("before fork\n");
    
    if((pid = fork()) < 0 ){
        perror("fork: ");
        close(pid);
    } else if(pid > 0){
        wait(0);
        return 1;
    } else if(pid == 0){
        close(pipeOut[PIPE_IN]);

        
        if(toErr){
            printf("RRRRRRRRRRRRR\n");
            if (dup2(pipeOut[PIPE_OUT], STDERR_FILENO) < 0) {
                perror("In Task:  redirecting stdout");
            }
        } else {
            dup2(newsockfd, STDERR_FILENO);
        }
        
        if (dup2(pipeOut[PIPE_OUT], STDOUT_FILENO) < 0) {
             perror("In Task:  redirecting stdout");
        }

        if(execvp(cmd, params.data())<0){
            char *errMsg = "Unknown Command: ";
            char *fullMsg = strcat(errMsg, cmd); 
            printf("%s\n", fullMsg);
            _exit(EXIT_FAILURE);
        }
        
        _exit(EXIT_SUCCESS);
    }
}

void Task::printCmd(){
    queue<char*> tmp(command);
    printf("===================\n\n");
    while(!tmp.empty()){
        printf("%s\n", tmp.front());
        tmp.pop();
    }
    printf("delay: %d\n", pipeCnt);
    printf("===================\n\n");
}
