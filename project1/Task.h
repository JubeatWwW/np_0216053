#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <vector>
#include <iostream>
#include <string.h>
#include <sstream>
#include <queue>
#include <sys/wait.h>
using namespace std;


class Task{
    public:
        Task(queue<char*> command);
        int execute(queue<Task> pipeInput, int newsockfd);
        int pipeCnt;
        int pipeIn[2];
        int pipeOut[2];
        bool toPrint;
        bool toFile;
        bool toErr;
        char* filename;
        void printCmd();       
    private:
        queue<char*> command;
};
