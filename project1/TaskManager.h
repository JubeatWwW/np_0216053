#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <vector>
#include <iostream>
#include <string.h>
#include <sstream>
#include <fstream>
#include <queue>
using namespace std;

#include "Task.h"

class TaskManager{
    public:
        TaskManager();
        void pushTask(queue<Task> input, int newsockfd);
        int next();
    private:
        queue<Task> tasks[1023];
        void pipeTask(Task task);
        int head;
};

