#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <vector>
#include <iostream>
#include <string.h>
#include <sstream>
#include <queue>
using namespace std;

#include "Task.h"

class Parser{
    public:
        Parser(string command);
        queue<Task> parse();
        bool execute(); 
    private:
        queue<Task> commands;
        int status;
        string cmd;
        bool isFirstCmd;
        stringstream sstr;
        char* str2char(string str);
};
