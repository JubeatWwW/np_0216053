#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <queue>
#include <signal.h>

#include "Parser.h"
#include "TaskManager.h"

using namespace std;

#define PIPE_IN 0
#define PIPE_OUT 1

void error(const char *msg){
    perror(msg);
    exit(1);
}

void reaper(){
    
}

class Server {
    public:
        Server(int portno);
        void Accept();
    private:
        int sockfd;
        int newsockfd;
        int portno;
        int n;
        socklen_t clilen;
        char buffer[256];
        struct sockaddr_in serv_addr, cli_addr;
};

Server::Server(int portno){
    setenv("PATH", ".:bin", 1);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    this->portno = portno;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
        error("ERROR on binding");
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
    cout << "Listeing on port " << portno << endl;
}

void Server::Accept(){
    int pid;
    while(true){
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0) 
            error("ERROR on accept");

        if((pid = fork()) == -1){
            error("ERROR on fork()");
            close(newsockfd);
        } else if (pid > 0){
            close(newsockfd);
            waitpid(pid, NULL, 0);
        } else if(pid == 0){
            TaskManager taskmanager;
            char *welcome1 = "****************************************\n";
            char *welcome2 = "** Welcome to the information server. **\n";
            write(newsockfd, welcome1, strlen(welcome1));
            write(newsockfd, welcome2, strlen(welcome2));
            write(newsockfd, welcome1, strlen(welcome1));

            while(true){
                n = write(newsockfd, "% ",2);
                bzero(buffer,256);
                n = read(newsockfd,buffer,255);
                if (n < 0) error("ERROR reading from socket");
                string str(buffer);
                cout << "\n\nGot message: " << str << endl;
                   
                Parser parser(str);  
                queue<Task> tasks= parser.parse();

                taskmanager.pushTask(tasks, newsockfd);
                
                if (n < 0) error("ERROR writing to socket");
            }
            close(newsockfd);
            _exit(0);
            break;
        }
    }
    exit(0);
}

int main(int argc, char *argv[]){
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     int portno = atoi(argv[1]);

     Server serv(portno);
     serv.Accept();
     return 0; 

}
